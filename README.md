# Sinergia Foreign Data Wrapper for PostgreSQL

This is a foreign data wrapper for PostgreSQL, modeled after [blackhole_fdw](https://bitbucket.org/adunstan/blackhole_fdw). 
It exposes a KV store, allowing declarative SQL queries over it.

**Prerequisites:** PostgreSQL 9.4 - 9.5

**Installing:**

1. Move into sinergia_fdw directory
2. Run **make**
3. Start the postgres server
4. Start a postgres client
5. While in the psql shell, execute the following:


```
#!SQL

DROP EXTENSION sinergia_fdw CASCADE;

CREATE EXTENSION sinergia_fdw;

CREATE SERVER sinergia_server 
    FOREIGN DATA WRAPPER sinergia_fdw 
    OPTIONS (address 'http://clusterinfo.unineuchatel.ch', port '10006');

CREATE FOREIGN TABLE sinergia_db0 (key text, value text) 
    SERVER sinergia_server;

CREATE USER MAPPING FOR PUBLIC
    SERVER sinergia_server;
```

**Running queries:**

* Inserting data:

```
#!SQL
INSERT INTO sinergia_db0 VALUES('key','value');

```
* Querying data - Example:

```
#!SQL
SELECT * FROM sinergia_db0 WHERE key < '12000';
```