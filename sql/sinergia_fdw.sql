/*-------------------------------------------------------------------------
 *				  foreign-data wrapper 	sinergia
 * Blueprint:
 *                foreign-data wrapper  blackhole
 *
 * Copyright (c) 2013, PostgreSQL Global Development Group
 *
 * This software is released under the PostgreSQL Licence
 *
 * Author:  Andrew Dunstan <andrew@dunslane.net>
 *
 * IDENTIFICATION
 *                sinergia_fdw/=sql/sinergia_fdw.sql
 *
 *-------------------------------------------------------------------------
 */

CREATE FUNCTION sinergia_fdw_handler()
RETURNS fdw_handler
AS 'MODULE_PATHNAME'
LANGUAGE C STRICT;

CREATE FUNCTION sinergia_fdw_validator(text[], oid)
RETURNS void
AS 'MODULE_PATHNAME'
LANGUAGE C STRICT;

CREATE FOREIGN DATA WRAPPER sinergia_fdw
  HANDLER sinergia_fdw_handler
  VALIDATOR sinergia_fdw_validator;
