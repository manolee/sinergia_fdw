/*-------------------------------------------------------------------------
 *
 * Foreign Data wrapper for KV store of UniNe
 *
 * Blueprint:
 * Blackhole Foreign Data Wrapper for PostgreSQL
 *
 * Copyright (c) 2013 Andrew Dunstan
 *
 * This software is released under the PostgreSQL Licence
 *
 * Author: Andrew Dunstan <andrew@dunslane.net>
 *
 * IDENTIFICATION
 *        sinergia_fdw/src/sinergia_fdw.c
 *
 *-------------------------------------------------------------------------
 */

#include "postgres.h"

#include "funcapi.h"
#include "miscadmin.h"
#include <assert.h>
#include "access/reloptions.h"
#include "foreign/fdwapi.h"
#include "foreign/foreign.h"
#include "catalog/pg_foreign_server.h"
#include "optimizer/pathnode.h"
#include "optimizer/planmain.h"
#include "optimizer/restrictinfo.h"
#include "lib/stringinfo.h"
#include "utils/rel.h"
#include "utils/builtins.h"
#include "commands/defrem.h"


#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <curl/curl.h>
#define JSMN_STRICT
#define MAXTOKENS 256

//curl-related -> need a write buffer
#define MAXCURLRESPONSE 65536
char wr_buf[MAXCURLRESPONSE+1];

/**
 * JSMN
 */
#ifndef __JSMN_H_
#define __JSMN_H_

#ifdef __cplusplus
extern "C" {
#endif

/**
 * JSON type identifier. Basic types are:
 * 	o Object
 * 	o Array
 * 	o String
 * 	o Other primitive: number, boolean (true/false) or null
 */
typedef enum {
	JSMN_PRIMITIVE = 0,
	JSMN_OBJECT = 1,
	JSMN_ARRAY = 2,
	JSMN_STRING = 3
} jsmntype_t;

typedef enum {
	/* Not enough tokens were provided */
	JSMN_ERROR_NOMEM = -1,
	/* Invalid character inside JSON string */
	JSMN_ERROR_INVAL = -2,
	/* The string is not a full JSON packet, more bytes expected */
	JSMN_ERROR_PART = -3,
} jsmnerr_t;

/**
 * JSON token description.
 * @param		type	type (object, array, string etc.)
 * @param		start	start position in JSON data string
 * @param		end		end position in JSON data string
 */
typedef struct {
	jsmntype_t type;
	int start;
	int end;
	int size;
#ifdef JSMN_PARENT_LINKS
	int parent;
#endif
} jsmntok_t;

/**
 * JSON parser. Contains an array of token blocks available. Also stores
 * the string being parsed now and current position in that string
 */
typedef struct {
	unsigned int pos; /* offset in the JSON string */
	unsigned int toknext; /* next token to allocate */
	int toksuper; /* superior token node, e.g parent object or array */
} jsmn_parser;

/**
 * Create JSON parser over an array of tokens
 */
void jsmn_init(jsmn_parser *parser);

/**
 * Run JSON parser. It parses a JSON data string into and array of tokens, each describing
 * a single JSON object.
 */
jsmnerr_t jsmn_parse(jsmn_parser *parser, const char *js, size_t len,
		jsmntok_t *tokens, unsigned int num_tokens);

jsmntok_t * json_tokenise(char *js);


#ifdef __cplusplus
}
#endif

#endif /* __JSMN_H_ */

PG_MODULE_MAGIC;

typedef struct kvReply {
    size_t elementsNo; /* number of elements*/
    char **results; /* elements vector */
} kvReply;

/*
 * SQL functions
 */
extern Datum sinergia_fdw_handler(PG_FUNCTION_ARGS);
extern Datum sinergia_fdw_validator(PG_FUNCTION_ARGS);

PG_FUNCTION_INFO_V1(sinergia_fdw_handler);
PG_FUNCTION_INFO_V1(sinergia_fdw_validator);

/* protos */
static kvReply* sinergiaGetValue(char* address, int port,
		char* key) __attribute__((unused));

static void sinergiaGetQual(Node *node, TupleDesc tupdesc, char **key,
		char **value, bool *pushdown) __attribute__((unused));

/* callback functions */
static void sinergiaGetForeignRelSize(PlannerInfo *root,
						   RelOptInfo *baserel,
						   Oid foreigntableid);

static void sinergiaGetForeignPaths(PlannerInfo *root,
						 RelOptInfo *baserel,
						 Oid foreigntableid);

static ForeignScan *sinergiaGetForeignPlan(PlannerInfo *root,
						RelOptInfo *baserel,
						Oid foreigntableid,
						ForeignPath *best_path,
						List *tlist,
						List *scan_clauses);

static void sinergiaBeginForeignScan(ForeignScanState *node,
						  int eflags);

static TupleTableSlot *sinergiaIterateForeignScan(ForeignScanState *node);

static void sinergiaReScanForeignScan(ForeignScanState *node);

static void sinergiaEndForeignScan(ForeignScanState *node);

static List *sinergiaPlanForeignModify(PlannerInfo *root,
						   ModifyTable *plan,
						   Index resultRelation,
						   int subplan_index);

static void sinergiaBeginForeignModify(ModifyTableState *mtstate,
							ResultRelInfo *rinfo,
							List *fdw_private,
							int subplan_index,
							int eflags);

static TupleTableSlot *sinergiaExecForeignInsert(EState *estate,
						   ResultRelInfo *rinfo,
						   TupleTableSlot *slot,
						   TupleTableSlot *planSlot);

static void sinergiaEndForeignModify(EState *estate,
						  ResultRelInfo *rinfo);


/*
 * structures used by the FDW
 */


/*
 * Describes the valid options for objects that use this wrapper.
 */
struct SinergiaFdwOption
{
	const char *optname;
	Oid			optcontext;		/* Oid of catalog in which option may appear */
};

/*
 * Valid options for sinergia_fdw.
 *
 */
static struct SinergiaFdwOption valid_options[] =
{

	/* Connection options */
	{"address", ForeignServerRelationId},
	{"port", ForeignServerRelationId},

	/* Sentinel */
	{NULL, InvalidOid}
};

typedef struct sinergiaTableOptions
{
	char *address;
	int port;
} sinergiaTableOptions, *SinergiaTableOptions;

/*
 * This is what will be set and stashed away in fdw_private and fetched
 * for subsequent routines.
 */
typedef struct
{
	char	   *svr_address;
	int			svr_port;
}	SinergiaFdwPlanState;

/*
 * FDW-specific information for ForeignScanState.fdw_state.
 */

typedef struct SinergiaFdwExecutionState
{
	AttInMetadata *attinmeta;
	kvReply	   *kvReply;
	long long	row;
	char	   *address;
	int			port;
	char       *qual_value;
}	SinergiaFdwExecutionState;

/*
 * Helper functions
 */
static bool sinergiaIsValidOption(const char *option, Oid context);
static void sinergiaGetOptions(Oid foreigntableid, SinergiaTableOptions
		options);
static kvReply* sinergiaGetKeys(char* address, int port);

Datum
sinergia_fdw_handler(PG_FUNCTION_ARGS)
{
	FdwRoutine *fdwroutine = makeNode(FdwRoutine);

	elog(NOTICE,"entering function %s",__func__);

	/* assign the handlers for the FDW */

	/* these are required */
	fdwroutine->GetForeignRelSize = sinergiaGetForeignRelSize;
	fdwroutine->GetForeignPaths = sinergiaGetForeignPaths;
	fdwroutine->GetForeignPlan = sinergiaGetForeignPlan;
	fdwroutine->BeginForeignScan = sinergiaBeginForeignScan;
	fdwroutine->IterateForeignScan = sinergiaIterateForeignScan;
	fdwroutine->ReScanForeignScan = sinergiaReScanForeignScan;
	fdwroutine->EndForeignScan = sinergiaEndForeignScan;

	/* remainder are optional - use NULL if not required */
	/* support for insert / update / delete */

	fdwroutine->PlanForeignModify = sinergiaPlanForeignModify;
	fdwroutine->BeginForeignModify = sinergiaBeginForeignModify;
	fdwroutine->ExecForeignInsert = sinergiaExecForeignInsert;
	fdwroutine->EndForeignModify = sinergiaEndForeignModify;
	fdwroutine->ExecForeignUpdate = NULL;
	fdwroutine->ExecForeignDelete = NULL;
	fdwroutine->AddForeignUpdateTargets = NULL;


	/* support for EXPLAIN */
	fdwroutine->ExplainForeignScan = NULL;
	fdwroutine->ExplainForeignModify = NULL;

	/* support for ANALYSE */
	fdwroutine->AnalyzeForeignTable = NULL;

	PG_RETURN_POINTER(fdwroutine);
}

/*
 * Validate the generic options given to a FOREIGN DATA WRAPPER, SERVER,
 * USER MAPPING or FOREIGN TABLE that uses file_fdw.
 *
 * Raise an ERROR if the option or its value is considered invalid.
 */
Datum
sinergia_fdw_validator(PG_FUNCTION_ARGS)
{
	List	   *options_list = untransformRelOptions(PG_GETARG_DATUM(0));
	Oid			catalog = PG_GETARG_OID(1);
	char	   *svr_address = NULL;
	int			svr_port = 0;
	ListCell   *cell;

#ifdef DEBUG
	elog(NOTICE, "sinergia_fdw_validator");
#endif

	/*
	 * Check that only options supported by sinergia_fdw, and allowed for the
	 * current object type, are given.
	 */
	foreach(cell, options_list)
	{
		DefElem    *def = (DefElem *) lfirst(cell);

		if (!sinergiaIsValidOption(def->defname, catalog))
		{
			struct SinergiaFdwOption *opt;
			StringInfoData buf;

			/*
			 * Unknown option specified, complain about it. Provide a hint
			 * with list of valid options for the object.
			 */
			initStringInfo(&buf);
			for (opt = valid_options; opt->optname; opt++)
			{
				if (catalog == opt->optcontext)
					appendStringInfo(&buf, "%s%s", (buf.len > 0) ? ", " : "",
									 opt->optname);
			}

			ereport(ERROR,
					(errcode(ERRCODE_FDW_INVALID_OPTION_NAME),
					 errmsg("invalid option \"%s\"", def->defname),
					 errhint("Valid options in this context are: %s",
							 buf.len ? buf.data : "<none>")
					 ));
		}

		if (strcmp(def->defname, "address") == 0)
		{
			if (svr_address)
				ereport(ERROR, (errcode(ERRCODE_SYNTAX_ERROR),
								errmsg("conflicting or redundant options: "
									   "address (%s)", defGetString(def))
								));

			svr_address = defGetString(def);
			printf("%s\n",svr_address);


		}
		else if (strcmp(def->defname, "port") == 0)
		{
			if (svr_port)
				ereport(ERROR,
						(errcode(ERRCODE_SYNTAX_ERROR),
						 errmsg("conflicting or redundant options: port (%s)",
								defGetString(def))
						 ));

			svr_port = atoi(defGetString(def));
			printf("%d\n",svr_port);

		}
	}

	PG_RETURN_VOID();
}

/*
 * Check if the provided option is one of the valid options.
 * context is the Oid of the catalog holding the object the option is for.
 */
static bool
sinergiaIsValidOption(const char *option, Oid context)
{
	struct SinergiaFdwOption *opt;

#ifdef DEBUG
	elog(NOTICE, "sinergiaIsValidOption");
#endif

	for (opt = valid_options; opt->optname; opt++)
	{
		if (context == opt->optcontext && strcmp(opt->optname, option) == 0)
			return true;
	}
	return false;
}

/*
 * Fetch the options for a sinergia_fdw foreign table.
 */
static void
sinergiaGetOptions(Oid foreigntableid,SinergiaTableOptions table_options)
{
	ForeignTable *table;
	ForeignServer *server;
	UserMapping *mapping;
	List	   *options;
	ListCell   *lc;

#ifdef DEBUG
	elog(NOTICE, "sinergiaGetOptions");
#endif

	/*
	 * Extract options from FDW objects. We only need to worry about server
	 * options for Sinergia
	 *
	 */
	table = GetForeignTable(foreigntableid);
	server = GetForeignServer(table->serverid);
	mapping = GetUserMapping(GetUserId(), table->serverid);

	options = NIL;
	options = list_concat(options, table->options);
	options = list_concat(options, server->options);
	options = list_concat(options, mapping->options);

//	table_options->table_type = PG_SINERGIA_SCALAR_TABLE;

	/* Loop through the options, and get the server/port */
	foreach(lc, options)
	{
		DefElem    *def = (DefElem *) lfirst(lc);

		if (strcmp(def->defname, "address") == 0)
			table_options->address = defGetString(def);

		if (strcmp(def->defname, "port") == 0)
			table_options->port = atoi(defGetString(def));
	}

	/* Default values, if required */
	if (!table_options->address)
		table_options->address = "127.0.0.1";

	if (!table_options->port)
		table_options->port = 30001;
}

static void fetch_json(char *address, int port)
{
	char* cmd_part1 = "wget --method=KEYS ";
	char* cmd_part3 = " -O /tmp/keys.json";
	int size_address = 0;
	char buffer[10];
	int bufferLen = 0;
	int cmd_len = 0;
	char* cmd_entire = NULL;

	/* skip fetching if we have it locally */
	struct stat statbuf;
	const char* keysFilename = "/tmp/keys.json";

	if (stat (keysFilename, &statbuf) == 0)
		return;

	//Calculate size using the KEYS call
	//wget --method=KEYS http://clusterinfo.unineuchatel.ch:10006/ -O keys.json
	size_address = strlen(address);
	memset(buffer, '\0', 10);
	bufferLen = sprintf(buffer, "%d", port);
	cmd_len = strlen(cmd_part1) + size_address + bufferLen + 1 + 1
			+ strlen(cmd_part3);
	cmd_entire = (char*) malloc(cmd_len * sizeof(char));
	memset(cmd_entire, '\0', cmd_len);
	strcpy(cmd_entire, cmd_part1);
	strcat(cmd_entire, address);
	strcat(cmd_entire, ":");
	strcat(cmd_entire, buffer);
	strcat(cmd_entire, "/");
	strcat(cmd_entire, cmd_part3);
	system(cmd_entire);
}


static kvReply *parse_json()
{
	struct stat statbuf;
	const char* keysFilename = "/tmp/keys.json";
	size_t fsize = 0;
	int fd = -1;
	char *fileBuf = NULL;
	int i = 0;
	int keys = 0;
	//Actual keys handling
	int keyLen = 0;
	char **keysArray = NULL;
	kvReply *reply = NULL;
	jsmntok_t *tokens = NULL;

	//Next step: parse using jsmn
	stat(keysFilename, &statbuf);
	fsize = statbuf.st_size;
	fd = open(keysFilename, O_RDONLY);
	if (fd == -1)
	{
		exit(-1);
	}
	fileBuf = (char*) mmap(NULL, fsize, PROT_READ, MAP_PRIVATE, fd, 0);
	if (fileBuf == MAP_FAILED )
	{
		elog(NOTICE, "mmap error");
		exit(-1);
	}

	//Populating our json 'positional index'
	tokens = json_tokenise(fileBuf);
	if (tokens == NULL )
	{
		elog(NOTICE, "malloc error");
		exit(-1);
	}

	//Assuming 'input' is entirely flat
	if (tokens->type != JSMN_ARRAY)
	{
		elog(NOTICE, "jsmn error");
		exit(-1);
	}
	else
	{
		keys = tokens->size;
	}

	/**
	 * Note from jsmn:
	 * String tokens point to the first character after the opening quote
	 * and the previous symbol before final quote.
	 * This was made to simplify string extraction from JSON data.
	 */
	keysArray = (char**) malloc(keys * sizeof(char*));
	if (keysArray == NULL )
	{
		elog(NOTICE, "malloc error");
		exit(-1);
	}
	for (i = 1; i <= keys; i++)
	{
		if (tokens[i].type != JSMN_STRING && tokens[i].type != JSMN_PRIMITIVE)
		{
			elog(NOTICE, "unexpected key");
			exit(-1);
		}
		else
		{

			keyLen = (tokens[i].end - tokens[i].start);
			keysArray[i - 1] = (char*) malloc((keyLen + 1) * sizeof(char));
			if (keysArray[i - 1] == NULL )
			{
				elog(NOTICE, "malloc error");
				exit(-1);
			}
			memcpy(keysArray[i - 1], fileBuf + tokens[i].start, keyLen);
			keysArray[i - 1][keyLen] = '\0';
			//			printf("%s\n",keysArray[i-1]);
		}
	}

	reply = (kvReply*) malloc(sizeof(kvReply));
	reply->elementsNo = keys;
	reply->results = keysArray;
	return reply;
}

static void remove_file(const char *filename)
{
	char *cmd;

	cmd = calloc(strlen("rm ") /* avoid magic */ + strlen(filename) + 1,
				sizeof(char));

	sprintf(cmd, "rm %s", filename);

	system(cmd);

	free(cmd);
}

static kvReply* sinergiaGetKeys(char* address, int port)
{
	struct stat statbuf;
	const char* keysFilename = "/tmp/keys.json";
	kvReply *reply;

	if (stat (keysFilename, &statbuf) != 0) {
		printf("JSON file not found. Fetching...\n");
		fetch_json(address, port);
	}

	reply = parse_json();

	remove_file(keysFilename);

	return reply;
}

static void sinergia_put(char *address, int port, char *key, char *value)
{
	CURL *curl;
	CURLcode res;

	char buffer[10];
	int bufferLen = 0;
	char* url_entire = NULL;
	int cmd_len;

	memset(buffer, '\0', 10);
	bufferLen = sprintf(buffer, "%d", port);

	cmd_len = strlen(address) + 1 + bufferLen + 1 +
		strlen(key)+ 1;//':','/','\0'

	url_entire = (char*) malloc(cmd_len * sizeof(char));
	memset(url_entire, '\0', cmd_len);

	strcat(url_entire, address);
	strcat(url_entire, ":");
	strcat(url_entire, buffer);
	strcat(url_entire, "/");
	strcat(url_entire, key);

	printf("URL is %s\n", url_entire);

	/* In windows, this will init the winsock stuff */
	curl_global_init(CURL_GLOBAL_ALL);

	/* get a curl handle */
	curl = curl_easy_init();
	if(curl) {
		curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "PUT");
		curl_easy_setopt(curl, CURLOPT_URL, url_entire);
		curl_easy_setopt(curl, CURLOPT_INFILESIZE, strlen(value) + 1);
		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, value); /* data goes here */

		res = curl_easy_perform(curl);
		if (res != CURLE_OK) {
			printf("curl_easy_perform() failed: %s\n",
					curl_easy_strerror(res));
		}

		curl_easy_cleanup(curl);
	}

	curl_global_cleanup();

}

static kvReply* sinergiaGetValue(char* address, int port, char* key)
{
	//Preparation for wget
	char* cmd_part1 = "wget --method=GET ";
	char* cmd_part3 = " -O ";
	int size_address = 0;
	char buffer[10];
	int bufferLen = 0;
	int cmd_len = 0;
	char* cmd_entire = NULL;
	//
	//Preparation for mmapping
	struct stat statbuf;
	const char* keysFilename = key;
	size_t fsize = 0;
	int fd = -1;
	char *fileBuf = NULL;
	char* elem = NULL;

	kvReply *reply = NULL;

	size_address = strlen(address);
	memset(buffer, '\0', 10);
	bufferLen = sprintf(buffer, "%d", port);
	cmd_len = strlen(cmd_part1) + size_address + 1 + bufferLen + 1 +
		strlen(key)+ strlen(cmd_part3) + strlen(key) + 1;//':','/','\0'

	cmd_entire = (char*) malloc(cmd_len * sizeof(char));
	memset(cmd_entire, '\0', cmd_len);
	strcpy(cmd_entire, cmd_part1);
	strcat(cmd_entire, address);
	strcat(cmd_entire, ":");
	strcat(cmd_entire, buffer);
	strcat(cmd_entire, "/");
	strcat(cmd_entire, key);
	strcat(cmd_entire, cmd_part3);
	strcat(cmd_entire, key);
	system(cmd_entire);
	printf("Entire GET command: %s\n",cmd_entire);

	reply = (kvReply*) malloc(sizeof(kvReply));
	//Next step: parse using jsmn
	stat(keysFilename, &statbuf);
	fsize = statbuf.st_size;
	fd = open(key, O_RDONLY);
	if (fd == -1)
	{
		printf("unable to open keysfile");
		exit(-1);
	}
	fileBuf = (char*) mmap(NULL, fsize, PROT_READ, MAP_PRIVATE, fd, 0);
	if (fileBuf == MAP_FAILED )
	{
		printf("mmap error");
		exit(-1);
	}

	elem = (char*) malloc((fsize+1)*sizeof(char));
	elem[fsize] = '\0';
	memcpy(elem,fileBuf,fsize);
	reply->elementsNo = 1;
	reply->results = (char**) malloc(1 * sizeof(char*));
	reply->results[0] = elem;
	//printf("[getValue: ] %s\n",fileBuf);
	//printf("getValue successful!\n");

	munmap(fileBuf, fsize);

	remove_file(key);

	return reply;
}


//curl handler
static size_t curlWriter(void* buf, size_t size, size_t nmemb)
{
	size_t res;

	printf("curl writer called with size %lu nmemb %lu\n", size, nmemb);

	memset(wr_buf,'\0',MAXCURLRESPONSE+1);

	/* truncate data to available buffer space */
	res = MAXCURLRESPONSE < size * nmemb ? MAXCURLRESPONSE : size * nmemb;

	memcpy(wr_buf, buf, res);

	return size * nmemb;
}

static kvReply* sinergiaGetValueCurl(char* address, int port, char* key)
{
	kvReply *reply = NULL;

	char buffer[10];
	int bufferLen = 0;
	char* url_entire;
	int cmd_len;
	CURLcode code, result;
	CURL *curl;

	cmd_len = strlen(address) + 1 + bufferLen + 1 +
			strlen(key)+ 1;//':','/','\0'

	memset(buffer, '\0', 10);
	bufferLen = sprintf(buffer, "%d", port);

	url_entire = (char*) malloc(cmd_len * sizeof(char));
	memset(url_entire, '\0', cmd_len);

	reply = (kvReply*) malloc(sizeof(kvReply));
	reply->elementsNo = 0;
	reply->results = NULL;

	strcat(url_entire, address);
	strcat(url_entire, ":");
	strcat(url_entire, buffer);
	strcat(url_entire, "/");
	strcat(url_entire, key);

	printf("URL is %s\n", url_entire);

	result = curl_global_init(CURL_GLOBAL_NOTHING);
	if(result != CURLE_OK)
	{
		return reply;
	}

	curl = curl_easy_init();
	if(!curl)
	{
		return reply;
	}

	curl_easy_setopt(curl, CURLOPT_URL, url_entire);
	curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "GET");
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &curlWriter);

	code = curl_easy_perform(curl);

	//printf( "ret = %d\n", code);

  	/* Emit the page if curl indicates that no errors occurred */
  	if ( code == 0 ) {
  		//printf( "%s\n", wr_buf );
  		reply->elementsNo = 1;
		reply->results = (char**) malloc(sizeof(char*));
		reply->results[0] = wr_buf;
  	}	else	{
  		printf("Error code detected!\n");
  	}

	curl_easy_cleanup(curl);
	curl_global_cleanup();

#if 0
	reply = (kvReply*) malloc(sizeof(kvReply));
	reply->elementsNo = 1;
	memset(wr_buf, 0, MAXCURLRESPONSE);
	sprintf(wr_buf, "%s", key);
	reply->results = (char**) malloc(sizeof(char*));
	reply->results[0] = wr_buf;
#endif

	return reply;
}

static void
sinergiaGetForeignRelSize(PlannerInfo *root,
						   RelOptInfo *baserel,
						   Oid foreigntableid)
{
	/*
	 * Obtain relation size estimates for a foreign table. This is called at
	 * the beginning of planning for a query that scans a foreign table. root
	 * is the planner's global information about the query; baserel is the
	 * planner's information about this table; and foreigntableid is the
	 * pg_class OID of the foreign table. (foreigntableid could be obtained
	 * from the planner data structures, but it's passed explicitly to save
	 * effort.)
	 *
	 * This function should update baserel->rows to be the expected number of
	 * rows returned by the table scan, after accounting for the filtering
	 * done by the restriction quals. The initial value of baserel->rows is
	 * just a constant default estimate, which should be replaced if at all
	 * possible. The function may also choose to update baserel->width if it
	 * can compute a better estimate of the average result row width.
	 */

	SinergiaFdwPlanState *fdw_private;
	struct kvReply *reply;
	int i;
	sinergiaTableOptions table_options;

	elog(NOTICE,"entering function %s",__func__);

	fdw_private = palloc0(sizeof(SinergiaFdwPlanState));
	baserel->fdw_private = (void *) fdw_private;

	/* initialize required state in fdw_private */

	/*
	* Fetch options. Get everything so we don't need to re-fetch later in
	* planning.
	*/
	baserel->fdw_private = (void *) fdw_private;
	table_options.address = NULL;
	table_options.port = 0;
	sinergiaGetOptions(foreigntableid, &table_options);
	fdw_private->svr_address = table_options.address;
	fdw_private->svr_port = table_options.port;

	reply = sinergiaGetKeys(table_options.address, table_options.port);
	baserel->rows = reply->elementsNo;

	/* free all */
	for(i = 0; i < reply->elementsNo; i++)	{
		free(reply->results[i]);
	}
	free(reply->results);
	free(reply);
}

static void
sinergiaGetForeignPaths(PlannerInfo *root,
						 RelOptInfo *baserel,
						 Oid foreigntableid)
{
	/*
	 * Create possible access paths for a scan on a foreign table. This is
	 * called during query planning. The parameters are the same as for
	 * GetForeignRelSize, which has already been called.
	 *
	 * This function must generate at least one access path (ForeignPath node)
	 * for a scan on the foreign table and must call add_path to add each such
	 * path to baserel->pathlist. It's recommended to use
	 * create_foreignscan_path to build the ForeignPath nodes. The function
	 * can generate multiple access paths, e.g., a path which has valid
	 * pathkeys to represent a pre-sorted result. Each access path must
	 * contain cost estimates, and can contain any FDW-private information
	 * that is needed to identify the specific scan method intended.
	 */

	SinergiaFdwPlanState *fdw_private = baserel->fdw_private;

		Cost		startup_cost,
					total_cost;

#ifdef DEBUG
	elog(NOTICE, "sinergiaGetForeignPaths");
#endif

	if (strcmp(fdw_private->svr_address, "127.0.0.1") == 0 ||
		strcmp(fdw_private->svr_address, "localhost") == 0)
		startup_cost = 10;
	else
		startup_cost = 25;

	total_cost = startup_cost + baserel->rows;


	/* Create a ForeignPath node and add it as only possible path */
	add_path(baserel, (Path *)
			 create_foreignscan_path(root, baserel,
									 baserel->rows,
									 startup_cost,
									 total_cost,
									 NIL,		/* no pathkeys */
									 NULL,		/* no outer rel either */
									 NIL));		/* no fdw_private data */
}



static ForeignScan *
sinergiaGetForeignPlan(PlannerInfo *root,
						RelOptInfo *baserel,
						Oid foreigntableid,
						ForeignPath *best_path,
						List *tlist,
						List *scan_clauses)
{
	/*
	 * Create a ForeignScan plan node from the selected foreign access path.
	 * This is called at the end of query planning. The parameters are as for
	 * GetForeignRelSize, plus the selected ForeignPath (previously produced
	 * by GetForeignPaths), the target list to be emitted by the plan node,
	 * and the restriction clauses to be enforced by the plan node.
	 *
	 * This function must create and return a ForeignScan plan node; it's
	 * recommended to use make_foreignscan to build the ForeignScan node.
	 *
	 */

	Index		scan_relid = baserel->relid;

	/*
	 * We have no native ability to evaluate restriction clauses, so we just
	 * put all the scan_clauses into the plan node's qual list for the
	 * executor to check. So all we have to do here is strip RestrictInfo
	 * nodes from the clauses and ignore pseudoconstants (which will be
	 * handled elsewhere).
	 */

	elog(NOTICE,"entering function %s",__func__);

	scan_clauses = extract_actual_clauses(scan_clauses, false);

	/* Create the ForeignScan node */
	return make_foreignscan(tlist,
							scan_clauses,
							scan_relid,
							NIL,	/* no expressions to evaluate */
							NIL);		/* no private state either */

}

static void
sinergiaGetQual(Node *node, TupleDesc tupdesc, char **key, char **value,
		bool *pushdown)
{
	*key = NULL;
	*value = NULL;
	*pushdown = false;

	if (!node)
		return;

	if (IsA(node, OpExpr))
	{
		OpExpr	   *op = (OpExpr *) node;
		Node	   *left,
				   *right;
		Index		varattno;

		if (list_length(op->args) != 2)
			return;

		left = list_nth(op->args, 0);

		if (!IsA(left, Var))
			return;

		varattno = ((Var *) left)->varattno;

		right = list_nth(op->args, 1);

		if (IsA(right, Const))
		{
			StringInfoData buf;

			initStringInfo(&buf);

			/* And get the column and value... */
			*key = NameStr(tupdesc->attrs[varattno - 1]->attname);
			*value = TextDatumGetCString(((Const *) right)->constvalue);

			/*
			 * We can push down this qual if: - The operatory is TEXTEQ - The
			 * qual is on the key column
			 */
			if (strcmp(*key, "key") == 0)
				*pushdown = true;

			return;
		}
	}

	return;
}

//TODO
static void
sinergiaBeginForeignScan(ForeignScanState *node, int eflags)
{
	/*
	 * Begin executing a foreign scan. This is called during executor startup.
	 * It should perform any initialization needed before the scan can start,
	 * but not start executing the actual scan (that should be done upon the
	 * first call to IterateForeignScan). The ForeignScanState node has
	 * already been created, but its fdw_state field is still NULL.
	 * Information about the table to scan is accessible through the
	 * ForeignScanState node (in particular, from the underlying ForeignScan
	 * plan node, which contains any FDW-private information provided by
	 * GetForeignPlan). eflags contains flag bits describing the executor's
	 * operating mode for this plan node.
	 *
	 * Note that when (eflags & EXEC_FLAG_EXPLAIN_ONLY) is true, this function
	 * should not perform any externally-visible actions; it should only do
	 * the minimum required to make the node state valid for
	 * ExplainForeignScan and EndForeignScan.
	 *
	 */
	sinergiaTableOptions table_options;
	bool pushdown = false;
	char *qual_key __attribute__((unused));
	char *qual_value;
	SinergiaFdwExecutionState *festate;
	kvReply *reply;

	qual_key = qual_value = table_options.address = NULL;
	table_options.port = 0;

	/* Fetch options  */
	sinergiaGetOptions(RelationGetRelid(node->ss.ss_currentRelation),
			&table_options);

	elog(NOTICE,"entering function %s",__func__);

//	/* See if we've got a qual we can push down */
//	if (node->ss.ps.plan->qual)
//	{
//		ListCell *lc;
//
//		foreach(lc, node->ss.ps.qual)
//		{
//			/* Only the first qual can be pushed down to Redis */
//			ExprState *state = lfirst(lc);
//
//			sinergiaGetQual((Node *) state->expr,
//					node->ss.ss_currentRelation->rd_att, &qual_key, &qual_value,
//					&pushdown);
//			if (pushdown)
//				break;
//		}
//		printf("HM?\n");
//		printf("%s\n",qual_key);
//		printf("%s\n",qual_value);
//	}

	//system()
	//wget --method=KEYS http://clusterinfo.unineuchatel.ch:10006/
	//	-O allkeys.json

	festate = (SinergiaFdwExecutionState *)
					palloc(sizeof(SinergiaFdwExecutionState));

	node->fdw_state = (void *) festate;
	festate->kvReply = NULL;
	festate->row = 0;
	festate->address = table_options.address;
	festate->port = table_options.port;
	festate->qual_value = pushdown ? qual_value : NULL;

	reply = sinergiaGetKeys(festate->address,festate->port);
	printf("Will iterate over %ld values\n",reply->elementsNo);
	festate->kvReply = reply;

	/* Store the additional state info */
	festate->attinmeta = TupleDescGetAttInMetadata(
			node->ss.ss_currentRelation->rd_att);
}


//TODO
static TupleTableSlot *
sinergiaIterateForeignScan(ForeignScanState *node)
{
	/*
	 * Fetch one row from the foreign source, returning it in a tuple table
	 * slot (the node's ScanTupleSlot should be used for this purpose). Return
	 * NULL if no more rows are available. The tuple table slot infrastructure
	 * allows either a physical or virtual tuple to be returned; in most cases
	 * the latter choice is preferable from a performance standpoint. Note
	 * that this is called in a short-lived memory context that will be reset
	 * between invocations. Create a memory context in BeginForeignScan if you
	 * need longer-lived storage, or use the es_query_cxt of the node's
	 * EState.
	 *
	 * The rows returned must match the column signature of the foreign table
	 * being scanned. If you choose to optimize away fetching columns that are
	 * not needed, you should insert nulls in those column positions.
	 *
	 * Note that PostgreSQL's executor doesn't care whether the rows returned
	 * violate any NOT NULL constraints that were defined on the foreign table
	 * columns ” but the planner does care, and may optimize queries
	 * incorrectly if NULL values are present in a column declared not to
	 * contain them. If a NULL value is encountered when the user has declared
	 * that none should be present, it may be appropriate to raise an error
	 * (just as you would need to do in the case of a data type mismatch).
	 */

	bool found;
	kvReply *reply = 0;
	char *key;
	char *data = 0;
	char **values;
	HeapTuple tuple;
	int i;

	SinergiaFdwExecutionState *festate =
			(SinergiaFdwExecutionState *) node->fdw_state;
	TupleTableSlot *slot = node->ss.ss_ScanTupleSlot;

	elog(NOTICE, "entering function %s", __func__);

	/* Cleanup */
	ExecClearTuple(slot);

	/* get the next record, if any, and fill in the slot */
	/* Get the next record, and set found */
	found = false;

	if (festate->row > -1 && festate->row < festate->kvReply->elementsNo)
	{
		key = festate->kvReply->results[festate->row];

		//Perform GET
		//reply = sinergiaGetValueCurl(festate->address, festate->port, key);
		reply = sinergiaGetValue(festate->address, festate->port, key);

		for (i = 0; i < reply->elementsNo; i++) {
			printf("key %s value %d is %s\n", key, i,
				reply->results[i]);
		}
	}
	festate->row++;

	if (festate->row <= festate->kvReply->elementsNo && reply->elementsNo > 0)
	{
		//Parse response
		//printf("%s\n",reply->results[0]);
		int valLen = (strlen(reply->results[0]) + 1);
		data = (char*) malloc(valLen * sizeof(char));
		data[valLen] = '\0';
		memcpy(data,reply->results[0],valLen);
		found = true;
	}

	/* Build the tuple */
	values = (char **) palloc(sizeof(char *) * 2);

	if (found)
	{
		values[0] = key;
		values[1] = data;

		tuple = BuildTupleFromCStrings(festate->attinmeta, values);
		ExecStoreTuple(tuple, slot, InvalidBuffer, false );
	}

	/* then return the slot */
	return slot;
}

//TODO
static void
sinergiaReScanForeignScan(ForeignScanState *node)
{
	/*
	 * Restart the scan from the beginning. Note that any parameters the scan
	 * depends on may have changed value, so the new scan does not necessarily
	 * return exactly the same rows.
	 */

	elog(NOTICE,"entering function %s",__func__);

}

//TODO
static void
sinergiaEndForeignScan(ForeignScanState *node)
{
	/*
	 * End the scan and release resources. It is normally not important to
	 * release palloc'd memory, but for example open files and connections to
	 * remote servers should be cleaned up.
	 */

	elog(NOTICE,"entering function %s",__func__);

}


static List *
sinergiaPlanForeignModify(PlannerInfo *root,
						   ModifyTable *plan,
						   Index resultRelation,
						   int subplan_index)
{
	/*
	 * Perform any additional planning actions needed for an insert, update,
	 * or delete on a foreign table. This function generates the FDW-private
	 * information that will be attached to the ModifyTable plan node that
	 * performs the update action. This private information must have the form
	 * of a List, and will be delivered to BeginForeignModify during the
	 * execution stage.
	 *
	 * root is the planner's global information about the query. plan is the
	 * ModifyTable plan node, which is complete except for the fdwPrivLists
	 * field. resultRelation identifies the target foreign table by its
	 * rangetable index. subplan_index identifies which target of the
	 * ModifyTable plan node this is, counting from zero; use this if you want
	 * to index into plan->plans or other substructure of the plan node.
	 *
	 * If the PlanForeignModify pointer is set to NULL, no additional
	 * plan-time actions are taken, and the fdw_private list delivered to
	 * BeginForeignModify will be NIL.
	 */

	elog(NOTICE,"entering function %s",__func__);

	return NULL;
}


static void
sinergiaBeginForeignModify(ModifyTableState *mtstate,
							ResultRelInfo *rinfo,
							List *fdw_private,
							int subplan_index,
							int eflags)
{
	/*
	 * Begin executing a foreign table modification operation. This routine is
	 * called during executor startup. It should perform any initialization
	 * needed prior to the actual table modifications. Subsequently,
	 * ExecForeignInsert, ExecForeignUpdate or ExecForeignDelete will be
	 * called for each tuple to be inserted, updated, or deleted.
	 *
	 * mtstate is the overall state of the ModifyTable plan node being
	 * executed; global data about the plan and execution state is available
	 * via this structure. rinfo is the ResultRelInfo struct describing the
	 * target foreign table. (The ri_FdwState field of ResultRelInfo is
	 * available for the FDW to store any private state it needs for this
	 * operation.) fdw_private contains the private data generated by
	 * PlanForeignModify, if any. subplan_index identifies which target of the
	 * ModifyTable plan node this is. eflags contains flag bits describing the
	 * executor's operating mode for this plan node.
	 *
	 * Note that when (eflags & EXEC_FLAG_EXPLAIN_ONLY) is true, this function
	 * should not perform any externally-visible actions; it should only do
	 * the minimum required to make the node state valid for
	 * ExplainForeignModify and EndForeignModify.
	 *
	 * If the BeginForeignModify pointer is set to NULL, no action is taken
	 * during executor startup.
	 */
	SinergiaFdwExecutionState *festate;
	sinergiaTableOptions table_options;

	elog(NOTICE,"entering function %s",__func__);

	/* Fetch options  */
	table_options.address = NULL;
	table_options.port = 0;

	sinergiaGetOptions(RelationGetRelid(rinfo->ri_RelationDesc),
			&table_options);

	/* set state */
	festate = (SinergiaFdwExecutionState *)
				palloc(sizeof(SinergiaFdwExecutionState));

	rinfo->ri_FdwState = (void *) festate;
	festate->kvReply = NULL;
	festate->row = 0;
	festate->address = table_options.address;
	festate->port = table_options.port;
}


static TupleTableSlot *
sinergiaExecForeignInsert(EState *estate,
						   ResultRelInfo *rinfo,
						   TupleTableSlot *slot,
						   TupleTableSlot *planSlot)
{
	/*
	 * Insert one tuple into the foreign table. estate is global execution
	 * state for the query. rinfo is the ResultRelInfo struct describing the
	 * target foreign table. slot contains the tuple to be inserted; it will
	 * match the rowtype definition of the foreign table. planSlot contains
	 * the tuple that was generated by the ModifyTable plan node's subplan; it
	 * differs from slot in possibly containing additional "junk" columns.
	 * (The planSlot is typically of little interest for INSERT cases, but is
	 * provided for completeness.)
	 *
	 * The return value is either a slot containing the data that was actually
	 * inserted (this might differ from the data supplied, for example as a
	 * result of trigger actions), or NULL if no row was actually inserted
	 * (again, typically as a result of triggers). The passed-in slot can be
	 * re-used for this purpose.
	 *
	 * The data in the returned slot is used only if the INSERT query has a
	 * RETURNING clause. Hence, the FDW could choose to optimize away
	 * returning some or all columns depending on the contents of the
	 * RETURNING clause. However, some slot must be returned to indicate
	 * success, or the query's reported rowcount will be wrong.
	 *
	 * If the ExecForeignInsert pointer is set to NULL, attempts to insert
	 * into the foreign table will fail with an error message.
	 *
	 */
	bool is_null;
	int nattrs;
	char *key, *value;
	SinergiaFdwExecutionState *festate;

	festate = (SinergiaFdwExecutionState *) rinfo->ri_FdwState;

	elog(NOTICE,"entering function %s",__func__);

	nattrs = RelationGetNumberOfAttributes(rinfo->ri_RelationDesc);
	assert (nattrs == 2);

	key = TextDatumGetCString(slot_getattr(slot, 1, &is_null));
	assert (!is_null);

	value = TextDatumGetCString(slot_getattr(slot, 2, &is_null));
	assert (!is_null);

	sinergia_put(festate->address, festate->port, key, value);

	return slot;
}



static void
sinergiaEndForeignModify(EState *estate,
						  ResultRelInfo *rinfo)
{
	/*
	 * End the table update and release resources. It is normally not
	 * important to release palloc'd memory, but for example open files and
	 * connections to remote servers should be cleaned up.
	 *
	 * If the EndForeignModify pointer is set to NULL, no action is taken
	 * during executor shutdown.
	 */

	elog(NOTICE,"entering function %s",__func__);

}


/**
 * JSMN
 */

/**
 * Allocates a fresh unused token from the token pull.
 */
static jsmntok_t *jsmn_alloc_token(jsmn_parser *parser,
		jsmntok_t *tokens, size_t num_tokens) {
	jsmntok_t *tok;
	if (parser->toknext >= num_tokens) {
		return NULL;
	}
	tok = &tokens[parser->toknext++];
	tok->start = tok->end = -1;
	tok->size = 0;
#ifdef JSMN_PARENT_LINKS
	tok->parent = -1;
#endif
	return tok;
}

/**
 * Fills token type and boundaries.
 */
static void jsmn_fill_token(jsmntok_t *token, jsmntype_t type,
                            int start, int end) {
	token->type = type;
	token->start = start;
	token->end = end;
	token->size = 0;
}

/**
 * Fills next available token with JSON primitive.
 */
static jsmnerr_t jsmn_parse_primitive(jsmn_parser *parser, const char *js,
		size_t len, jsmntok_t *tokens, size_t num_tokens) {
	jsmntok_t *token;
	int start;

	start = parser->pos;

	for (; parser->pos < len && js[parser->pos] != '\0'; parser->pos++) {
		switch (js[parser->pos]) {
#ifndef JSMN_STRICT
			/* In strict mode primitive must be followed by "," or "}" or "]" */
			case ':':
#endif
			case '\t' : case '\r' : case '\n' : case ' ' :
			case ','  : case ']'  : case '}' :
				goto found;
		}
		if (js[parser->pos] < 32 || js[parser->pos] >= 127) {
			parser->pos = start;
			return JSMN_ERROR_INVAL;
		}
	}
#ifdef JSMN_STRICT
	/* In strict mode primitive must be followed by a comma/object/array */
	parser->pos = start;
	return JSMN_ERROR_PART;
#endif

found:
	if (tokens == NULL) {
		parser->pos--;
		return 0;
	}
	token = jsmn_alloc_token(parser, tokens, num_tokens);
	if (token == NULL) {
		parser->pos = start;
		return JSMN_ERROR_NOMEM;
	}
	jsmn_fill_token(token, JSMN_PRIMITIVE, start, parser->pos);
#ifdef JSMN_PARENT_LINKS
	token->parent = parser->toksuper;
#endif
	parser->pos--;
	return 0;
}

/**
 * Filsl next token with JSON string.
 */
static jsmnerr_t jsmn_parse_string(jsmn_parser *parser, const char *js,
		size_t len, jsmntok_t *tokens, size_t num_tokens) {

	int i;
	jsmntok_t *token;
	int start = parser->pos;

	parser->pos++;

	/* Skip starting quote */
	for (; parser->pos < len && js[parser->pos] != '\0'; parser->pos++) {
		char c = js[parser->pos];

		/* Quote: end of string */
		if (c == '\"') {
			if (tokens == NULL) {
				return 0;
			}
			token = jsmn_alloc_token(parser, tokens, num_tokens);
			if (token == NULL) {
				parser->pos = start;
				return JSMN_ERROR_NOMEM;
			}
			jsmn_fill_token(token, JSMN_STRING, start+1, parser->pos);
#ifdef JSMN_PARENT_LINKS
			token->parent = parser->toksuper;
#endif
			return 0;
		}

		/* Backslash: Quoted symbol expected */
		if (c == '\\') {
			parser->pos++;
			switch (js[parser->pos]) {
				/* Allowed escaped symbols */
				case '\"': case '/' : case '\\' : case 'b' :
				case 'f' : case 'r' : case 'n'  : case 't' :
					break;
				/* Allows escaped symbol \uXXXX */
				case 'u':
					parser->pos++;
					i = 0;
					for(; i < 4 && js[parser->pos] != '\0'; i++) {
						/* If it isn't a hex character we have an error */
						if(!((js[parser->pos] >= 48 &&
									js[parser->pos] <= 57) || /* 0-9 */
								(js[parser->pos] >= 65 &&
								 	js[parser->pos] <= 70) || /* A-F */
								(js[parser->pos] >= 97 &&
								 	js[parser->pos] <= 102))) { /* a-f */
							parser->pos = start;
							return JSMN_ERROR_INVAL;
						}
						parser->pos++;
					}
					parser->pos--;
					break;
				/* Unexpected symbol */
				default:
					parser->pos = start;
					return JSMN_ERROR_INVAL;
			}
		}
	}
	parser->pos = start;
	return JSMN_ERROR_PART;
}

/**
 * Parse JSON string and fill tokens.
 */
jsmnerr_t jsmn_parse(jsmn_parser *parser, const char *js, size_t len,
		jsmntok_t *tokens, unsigned int num_tokens) {
	jsmnerr_t r;
	int i;
	jsmntok_t *token;
	int count = 0;

	for (; parser->pos < len && js[parser->pos] != '\0'; parser->pos++) {
		char c;
		jsmntype_t type;

		c = js[parser->pos];
		switch (c) {
			case '{': case '[':
				count++;
				if (tokens == NULL) {
					break;
				}
				token = jsmn_alloc_token(parser, tokens, num_tokens);
				if (token == NULL)
					return JSMN_ERROR_NOMEM;
				if (parser->toksuper != -1) {
					tokens[parser->toksuper].size++;
#ifdef JSMN_PARENT_LINKS
					token->parent = parser->toksuper;
#endif
				}
				token->type = (c == '{' ? JSMN_OBJECT : JSMN_ARRAY);
				token->start = parser->pos;
				parser->toksuper = parser->toknext - 1;
				break;
			case '}': case ']':
				if (tokens == NULL)
					break;
				type = (c == '}' ? JSMN_OBJECT : JSMN_ARRAY);
#ifdef JSMN_PARENT_LINKS
				if (parser->toknext < 1) {
					return JSMN_ERROR_INVAL;
				}
				token = &tokens[parser->toknext - 1];
				for (;;) {
					if (token->start != -1 && token->end == -1) {
						if (token->type != type) {
							return JSMN_ERROR_INVAL;
						}
						token->end = parser->pos + 1;
						parser->toksuper = token->parent;
						break;
					}
					if (token->parent == -1) {
						break;
					}
					token = &tokens[token->parent];
				}
#else
				for (i = parser->toknext - 1; i >= 0; i--) {
					token = &tokens[i];
					if (token->start != -1 && token->end == -1) {
						if (token->type != type) {
							return JSMN_ERROR_INVAL;
						}
						parser->toksuper = -1;
						token->end = parser->pos + 1;
						break;
					}
				}
				/* Error if unmatched closing bracket */
				if (i == -1) return JSMN_ERROR_INVAL;
				for (; i >= 0; i--) {
					token = &tokens[i];
					if (token->start != -1 && token->end == -1) {
						parser->toksuper = i;
						break;
					}
				}
#endif
				break;
			case '\"':
				r = jsmn_parse_string(parser, js, len, tokens, num_tokens);
				if (r < 0) return r;
				count++;
				if (parser->toksuper != -1 && tokens != NULL)
					tokens[parser->toksuper].size++;
				break;
			case '\t' : case '\r' : case '\n' : case ':' : case ',': case ' ':
				break;
#ifdef JSMN_STRICT
			/* In strict mode primitives are: numbers and booleans */
			case '-': case '0': case '1' : case '2': case '3' : case '4':
			case '5': case '6': case '7' : case '8': case '9':
			case 't': case 'f': case 'n' :
#else
			/* In non-strict mode every unquoted value is a primitive */
			default:
#endif
				r = jsmn_parse_primitive(parser, js, len, tokens, num_tokens);
				if (r < 0) return r;
				count++;
				if (parser->toksuper != -1 && tokens != NULL)
					tokens[parser->toksuper].size++;
				break;

#ifdef JSMN_STRICT
			/* Unexpected char in strict mode */
			default:
				return JSMN_ERROR_INVAL;
#endif
		}
	}

	for (i = parser->toknext - 1; i >= 0; i--) {
		/* Unmatched opened object or array */
		if (tokens[i].start != -1 && tokens[i].end == -1) {
			return JSMN_ERROR_PART;
		}
	}

	return count;
}

/**
 * Creates a new parser based over a given  buffer with an array of tokens
 * available.
 */
void jsmn_init(jsmn_parser *parser) {
	parser->pos = 0;
	parser->toknext = 0;
	parser->toksuper = -1;
}

jsmntok_t * json_tokenise(char *js)
{
	unsigned int n;
	int i, ret;
	jsmntok_t *tokens;
	jsmn_parser parser;

	jsmn_init(&parser);

	n = MAXTOKENS;
	tokens = malloc(sizeof(jsmntok_t) * n);

	for (i = 0; i < n; i++)
	{
		tokens[i].start = 0;
		tokens[i].end = 0;
		tokens[i].size = 0;
	}

	ret = jsmn_parse(&parser, js, strlen(js), tokens, n);
	while (ret == JSMN_ERROR_NOMEM)
	{
		n = n * 2 + 1;
		tokens = realloc(tokens, sizeof(jsmntok_t) * n);
		for (i = 0; i < n; i++)
		{
			tokens[i].start = 0;
			tokens[i].end = 0;
			tokens[i].size = 0;
		}
		ret = jsmn_parse(&parser, js, strlen(js), tokens, n);
	}

	if (ret == JSMN_ERROR_INVAL)
	{
		elog(NOTICE,"jsmn_parse: invalid JSON string");
		exit(-1);
	}

	if (ret == JSMN_ERROR_PART)
	{
		elog(NOTICE, "jsmn_parse: truncated JSON string");
		exit(-1);
	}
	return tokens;
}


